# LOTR Skip

Skript to skip sam-scenes of a "certain" YT-Video:
https://www.youtube.com/watch?v=UHzF5KnoN20

**Just Copy the following Code**
```
javascript:var canvas=document.createElement("canvas");document.querySelector("body").appendChild(canvas);var video=document.querySelector("video:first-child"),videoWidth=video.offsetWidth,videoHeight=video.offsetHeight;canvas.width=videoWidth,canvas.height=videoHeight;var canvasContext=canvas.getContext("2d");redrawImage(),video.currentTime=2339.294;var jumpSeconds=24,compareRGB=[],pixelDataCompare="",counter=200,stopThis=!1;video.play(),video.playbackRate=.1,setInterval(function(){if(counter>0){if(isBuffered()){redrawImage();var e=getAverageRGB(),t=JSON.stringify(e);pixelDataCompare.includes(t)||(compareRGB.push(e),pixelDataCompare+=t),counter--}}else stopThis||(video.pause(),video.playbackRate=1,startInterval()),stopThis=!0},1);let timeout=0;function startInterval(){setInterval(function(){redrawImage();var e=getAverageRGB(),t=JSON.stringify(e);pixelDataCompare.includes(t)&&isBuffered()&&(video.currentTime=video.currentTime+jumpSeconds,timeout=5),timeout>0&&timeout--},10)}function getAverageRGB(){for(var e=canvasContext.getImageData(videoWidth-100,videoHeight-100,99,99).data,t=0,a=0,i=0,r=0,o=e.length/4;r<e.length;){var d=r%4;0==d?t+=e[r]:1==d?a+=e[r]:2==d&&(i+=e[r]),r++}return[t=Math.round(t/o),a=Math.round(a/o),i=Math.round(i/o)]}function isBuffered(){if(video.paused)return!1;let e=video.currentTime;for(i=0;i<video.buffered.length;i++)if(video.buffered.start(i)<e&&e+4<video.buffered.end(i))return!0;return!1}function redrawImage(){canvasContext.drawImage(video,0,0,videoWidth,videoHeight)}
```

Copy the script above and save it as shortcut (just like you save other pages). Click it once (when YouTube is open).  
When it starts it jumps to a certain scene, it slows down and plays the video. This is needed to compare the frames while the video is running.  
When finished it sets the video-speed to default and stops. Now you can watch and skip!  

## Description

Its not the best, but it's working - feel free to upgrade the code.  
<br>

**Short explanation:**  
I compare the frames of the video with the frames from the sam-scene and skip the sam-scene if it starts.  

**Long expanation:**  
So I took 100 x 100 px parts of the video (where the sam-scene starts) and calculate one average rgb value out of this part.  
I do this the first few frames (this is why the script jumps to the scene and plays it slow until it stops). <br>
Now the only thing to do is compare the current frames of the video with the frames of the example scene and jump the exact seconds forward.  
