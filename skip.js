// Create Video and Canvas-Element
var canvas = document.createElement("canvas");
document.querySelector("body").appendChild(canvas);
var video = document.querySelector('video:first-child');
var videoWidth = video.offsetWidth;
var videoHeight = video.offsetHeight;
canvas.width = videoWidth;
canvas.height = videoHeight;

// Draw Video in Canvas
var canvasContext = canvas.getContext('2d');
redrawImage();

// We jump to "38:59" this is where the first scene starts, then we check 5 pixels in the right bottom corner
// This is very exact time of the start
video.currentTime = 2339.294; 
// Scene-End 2364.5699
// So the difference is: 25.2758 Seconds, this is what we need to jump forward - made it to 24 - the other number didnt worked
var jumpSeconds = 24;
var compareRGB = [];

// Compare frames
var pixelDataCompare = "";
var counter = 200;
var stopThis = false;

// Start Code:
video.play();
video.playbackRate = 0.1;
setInterval(function(){
    if(counter>0){
        // Buffered?
        if(isBuffered()){
            redrawImage();
            var compareFrame = getAverageRGB();
            var compareString = JSON.stringify(compareFrame);

            if(!pixelDataCompare.includes(compareString)){
                compareRGB.push(compareFrame);
                pixelDataCompare+=compareString;
            }
            counter--;
        }
    }else{
        if(!stopThis){
           video.pause();
           video.playbackRate = 1;
           //console.log(pixelDataCompare);
           startInterval();
        }
        stopThis=true;
    }
}, 1);

let timeout = 0;
function startInterval(){
    // If we ever reach the point where the pixelData is the same, then we jump XX seconds in the future
    setInterval(function(){
        // Update Canvas
        redrawImage();
        // Get New Image-Frame to compare
        var compareFrame = getAverageRGB();
        var compareString = JSON.stringify(compareFrame);
    
        if(pixelDataCompare.includes(compareString) && isBuffered()){
            //console.log("true");
            video.currentTime = video.currentTime+jumpSeconds;
            timeout=5;
        }
         if(timeout>0){
            timeout--;
         }
    }, 10); 
    // Run this Code Every 10 ms this is very hard...
}

function getAverageRGB(){
    // Get image data right bottom square of image to compare
    var compareFrame = canvasContext.getImageData(videoWidth-100, videoHeight-100, 99, 99).data;
    var red = 0; // 0, 4, 8
    var green = 0; // 1, 5, 9
    var blue = 0; // 2, 6, 10

    var i = 0;
    var length = compareFrame.length;
    var lengthOfColor = length/4;
    while(i < compareFrame.length){
        var modulo = i % 4;
        if(modulo==0)
            red+=compareFrame[i];
        else if(modulo==1)
            green+=compareFrame[i];
        else if(modulo==2)
            blue+=compareFrame[i];
        i++;
    }

    red = Math.round(red/lengthOfColor);
    green = Math.round(green/lengthOfColor);
    blue = Math.round(blue/lengthOfColor);
    return [red,green,blue];
}

// Check if Video is buffered enough
function isBuffered(){
    if(video.paused)
        return false;

    let current = video.currentTime;
    // Check if Video is loaded 4 Seconds
    for(i = 0; i < video.buffered.length; i++)
        if(video.buffered.start(i) < current && current + 4 < video.buffered.end(i))
            return true;

    return false;
}

function redrawImage(){
    canvasContext.drawImage(video, 0, 0, videoWidth, videoHeight);
}